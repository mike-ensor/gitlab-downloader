#[cfg(test)]
mod test {
    use crate::cli_config::{CliError, CloneCommand, Command, CommonConfig};
    use std::borrow::Borrow;
    use std::env;

    #[test]
    fn test_default_config() {
        let expected_token = String::from("some-token-here");
        let input_path = String::from(".");

        let curr_dir = String::from(env::current_dir().unwrap().as_os_str().to_str().unwrap());

        let expected_path = curr_dir;

        let config = CommonConfig::new(expected_token.clone(), input_path.clone());

        assert_eq!(
            config.token, expected_token,
            "Both token values should be the same"
        );
        assert_eq!(
            config.os_path.unwrap(),
            expected_path,
            "Path should be CWD + Path"
        );
    }

    #[test]
    fn test_get_os_path() {
        let config_empty = CommonConfig::new("token".to_string(), "".to_string());
        let config_pwd = CommonConfig::new("token".to_string(), ".".to_string());
        let config_parent = CommonConfig::new("token".to_string(), "../".to_string());
        let config_abs = CommonConfig::new("token".to_string(), ".".to_string());
        let config_missing = CommonConfig::new(
            "token".to_string(),
            "/some-missing/cannot-have-this/directory".to_string(),
        );

        assert!(
            config_empty.os_path.is_ok(),
            "Failed on empty path, should default to ."
        );
        assert!(config_pwd.os_path.is_ok(), "Failed on ., should be legal");
        assert!(
            config_parent.os_path.is_ok(),
            "Failed on up a up a directory, tests are not run at root"
        );
        assert!(config_abs.os_path.is_ok(), "Failed on an absolute");
        assert!(
            config_missing.os_path.is_err(),
            "Failed on a missing directory to be error"
        );
    }

    #[test]
    fn test_cli_error_formatting() {
        let message = "this is generic".to_string();
        let general = CliError::General(message.clone());

        assert_eq!(general.to_string(), message)
    }

    #[test]
    fn test_custom_new() {
        let expected_token = String::from("some-token-here");
        let input_path = String::from(".");

        let config = CommonConfig::new_custom(
            expected_token.clone(),
            "gitlab.example.com".to_string(),
            true,
            true,
            true,
            input_path.clone(),
        );

        assert_eq!(
            config.insecure, true,
            "Passing in insecure=true, should be the same on output"
        );
    }

    #[test]
    fn test_clonecommand_default_new() {
        let config = CommonConfig::new("token-goes-here".to_string(), ".".to_string());

        let cmd = CloneCommand {
            group_id: 12345,
            max_depth: 0,
            config,
        };

        assert!(cmd.is_valid().is_ok())
    }

    #[test]
    fn test_clone_of_common_config() {
        // create one
        let config = CommonConfig::new("token-goes-here".to_string(), ".".to_string());

        // clone into a second
        let second = config.clone();

        // verify the same
        assert_eq!(config.quiet, second.quiet);
        assert!(config.os_path.is_ok());
        assert!(second.os_path.is_ok());
    }

    #[test]
    fn test_clone_of_clone_command() {}
}
