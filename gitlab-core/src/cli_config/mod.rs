mod mod_test;

use debug_print::{
    debug_eprint as deprint, debug_eprintln as deprintln, debug_print as dprint,
    debug_println as dprintln,
};

use crate::gitlab::gitlab_client::GitLabClient;
use crate::gitlab::GitLabErrors;
use log::debug;
use std::borrow::Borrow;
use std::error::Error;
use std::fmt::{Display, Formatter};
use std::io::Error as IoError;
use std::num::ParseIntError;
use std::path::Path;
use std::{env, fmt, num};

pub trait Command {
    /// Validates the command
    fn is_valid(&self) -> Result<(), CliError> {
        Ok(())
    }

    /// Operates as the action part of the command
    fn perform(&self) -> Result<(), CliError> {
        Ok(())
    }
}

/*************************************************************************/
/*************************************************************************/
/*************************************************************************/

#[derive(Debug)]
pub struct Update {}

#[derive(Debug)]
pub struct CloneCommand {
    pub group_id: usize,
    pub max_depth: usize,
    pub config: CommonConfig,
}

impl Command for Update {
    fn is_valid(&self) -> Result<(), CliError> {
        Ok(())
    }
}

// Clone Trait for CloneCommand
impl Clone for CloneCommand {
    fn clone(&self) -> CloneCommand {
        CloneCommand {
            group_id: self.group_id,
            config: self.config.clone(),
            max_depth: self.max_depth,
        }
    }

    fn clone_from(&mut self, source: &Self) {
        todo!("Future implementation")
    }
}

impl Command for CloneCommand {
    fn is_valid(&self) -> Result<(), CliError> {
        // List out all of the things that need to be valid
        if self.group_id == 0 {
            return Err(CliError::GroupId("Invalid Group ID".to_string()));
        }
        match self.config.is_valid() {
            Ok(_) => Ok(()),
            Err(e) => Err(e),
        }
    }

    fn perform(&self) -> Result<(), CliError> {
        // println!("Clone for: {} because is_valid = {}", self.group_id, self.is_valid());
        debug!("{:?}", self);
        let clone_command = CloneCommand {
            config: self.config.clone(),
            group_id: self.group_id,
            max_depth: self.max_depth,
        };
        let client = GitLabClient::new(clone_command);
        // let version_result = client.api_version();
        // let version = version_result.unwrap();
        // debug!(
        //     "GitLabAPI Version: {}, Revision: {}",
        //     version.version, version.revision
        // );

        let clone_result = client.clone_repos();

        if !clone_result.is_empty() {
            //UGH...I just want to output errors, just the first one (for now)
            let err: &GitLabErrors = clone_result.get(0).unwrap();
            Err(CliError::CloneError(err.to_string()))
        } else {
            Ok(())
        }
    }
}

/*************************************************************************/
/*************************************************************************/
/*************************************************************************/

#[derive(Debug)]
pub struct CommonConfig {
    // GitLab Personal Access Token
    pub token: String,
    // GitLab API host TLD
    pub host: String,
    // Use HTTP instead of HTTPS
    pub insecure: bool,
    // Perform operations or just output
    pub dry_run: bool,
    // Print output or reduce/eliminate
    pub quiet: bool,
    // Where to perform actions on OS
    pub os_path: Result<String, CliError>,
}

impl CommonConfig {
    /// Create a new Config object with defaults. Responsible for Validating the data
    /// For custom Config, see `from`
    pub fn new(token: String, os_path: String) -> CommonConfig {
        CommonConfig {
            token,
            host: "gitlab.com".to_owned(),
            insecure: false,
            dry_run: false,
            quiet: false,
            os_path: CommonConfig::get_os_path(os_path),
            /* Should this be a Result to capture if the OS path is possible?*/
        }
    }

    pub fn new_custom(
        token: String,
        host: String,
        insecure: bool,
        dry_run: bool,
        quiet: bool,
        os_path: String,
    ) -> CommonConfig {
        CommonConfig {
            token,
            host,
            insecure,
            dry_run,
            quiet,
            os_path: CommonConfig::get_os_path(os_path),
        }
    }

    fn get_os_path(mut input: String) -> Result<String, CliError> {
        let mut final_path = input.clone();
        // one special case, if empty, then it needs to be "."
        if input.is_empty() {
            input = ".".to_string();
        }
        let mut path = Path::new(input.as_str());

        if path.exists() {
            let final_path = match path.canonicalize() {
                Ok(s) => String::from(s.as_os_str().to_str().unwrap()),
                Err(e) => format!("Error discovering the absolute path for '{}'", input),
            };
            debug!("Final Path is `{}`", final_path);
            Ok(final_path)
        } else {
            Err(CliError::OSPath(format!(
                "Provided path '{}' is not a directory or is not writeable",
                input
            )))
        }
    }
}

// Clone Trait for CommonConfig
impl Clone for CommonConfig {
    fn clone(&self) -> CommonConfig {
        CommonConfig {
            token: self.token.to_string(),
            host: self.host.to_string(),
            insecure: self.insecure,
            dry_run: self.dry_run,
            quiet: self.quiet,
            os_path: self.os_path.clone(),
        }
    }

    fn clone_from(&mut self, source: &Self) {
        todo!("Future implementation when/if needed")
    }
}

impl Command for CommonConfig {
    // Valid means os_path is a real (writable) path, host is not empty and token is not empty
    fn is_valid(&self) -> Result<(), CliError> {
        if self.os_path.is_err() {
            let reference = self.os_path.as_ref();

            return match reference {
                Err(e) => {
                    return Err(CliError::OSPath(e.to_string()));
                }
                _ => Ok(()),
            };
        } else if self.host.is_empty() {
            return Err(CliError::Host("Provided host is not valid".to_string()));
        } else if self.token.is_empty() {
            return Err(CliError::General(
                "GitLab Personal Token needs to be provided".to_string(),
            ));
        }

        Ok(())
    }
}

/*********************************************************************************************/
/*********************************************************************************************/
/*********************************************************************************************/

#[derive(Debug, Clone)]
pub enum CliError {
    Host(String),
    GroupId(String),
    OSPath(String),
    Token(String),
    CloneError(String),
    General(String),
}

/// Output of Error text
impl Display for CliError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            CliError::Host(s) => write!(f, "{}", s),
            CliError::GroupId(s) => write!(f, "{}", s),
            CliError::OSPath(s) => write!(f, "{}", s),
            CliError::Token(s) => write!(f, "{}", s),
            CliError::CloneError(s) => write!(f, "{}", s),
            CliError::General(s) => write!(f, "{}", s),
        }
    }
}

impl Error for CliError {}

impl From<num::ParseIntError> for CliError {
    fn from(_: ParseIntError) -> Self {
        CliError::GroupId("Invalid Group ID".to_string())
    }
}

impl From<std::io::Error> for CliError {
    fn from(_: IoError) -> Self {
        CliError::OSPath("OS Path is not valid or is not writeable by user".to_string())
    }
}
