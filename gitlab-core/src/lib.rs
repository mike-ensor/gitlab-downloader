#![allow(unused, dead_code)]

use std::env;
use std::env::VarError;

pub mod cli_config;
pub mod gitlab;

pub fn get_env(key: &str) -> Result<String, String> {
    let key_result = env::var(key);
    match key_result {
        Ok(val) => Ok(val),
        Err(err) => Err(format!("ENV VAR {} was not found", key)),
    }
}

#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn test_get_env() {
        std::env::set_var("ENV1", "12345");

        let result = get_env("ENV1");

        assert!(result.is_ok());
        assert_eq!(result.unwrap(), "12345");
    }

    #[test]
    fn test_get_env_no_env() {
        assert!(get_env("SHOULD_NOT_EXIST_ANYWHERE").is_err());
    }
}
