use std::error::Error;
use std::fmt::{Display, Formatter};

// public
pub mod gitlab_client;
pub mod models;
// internal
mod gitlab_tests;
mod json_tests;

#[derive(Debug, Clone)]
pub enum GitLabErrors {
    HttpError(u16, String),
    Connection(String),
    FailedClone(String),
}

impl Display for GitLabErrors {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            GitLabErrors::HttpError(status_code, msg) => write!(f, "{msg} ({status_code})"),
            GitLabErrors::Connection(msg) => write!(f, "{msg}"),
            GitLabErrors::FailedClone(msg) => write!(f, "{msg}"),
        }
    }
}

impl Error for GitLabErrors {}
