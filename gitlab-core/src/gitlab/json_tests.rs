#[cfg(test)]
mod json_tests {
    use mockito::mock;
    use serde::{Deserialize, Serialize};

    use crate::gitlab::models::{Project, SubGroup, SubGroupProjects, Version};

    static SUBGROUP_JSON_RESPONSE: &str = r#"  {
        "id": 15143573,
        "web_url": "https://gitlab.com/groups/ultimate-top-five/customer-facing",
        "name": "customer-facing",
        "path": "customer-facing",
        "description": "",
        "visibility": "private",
        "share_with_group_lock": false,
        "require_two_factor_authentication": false,
        "two_factor_grace_period": 48,
        "project_creation_level": "developer",
        "auto_devops_enabled": null,
        "subgroup_creation_level": "maintainer",
        "emails_disabled": null,
        "mentions_disabled": null,
        "lfs_enabled": true,
        "default_branch_protection": 2,
        "avatar_url": "https://gitlab.com/uploads/-/system/group/avatar/15143573/customer-facing.png",
        "request_access_enabled": true,
        "full_name": "Ultimate Top Five Service / customer-facing",
        "full_path": "ultimate-top-five/customer-facing",
        "created_at": "2022-01-08T01:58:56.601Z",
        "parent_id": 15143439,
        "ldap_cn": null,
        "ldap_access": null,
        "marked_for_deletion_on": null
      }"#;

    static SUBGROUPS_JSON_RESPONSE: &str = r#"[{
        "id": 15158087,
        "web_url": "https://gitlab.com/groups/ultimate-top-five/admin",
        "name": "Admin",
        "path": "admin",
        "description": "",
        "visibility": "private",
        "share_with_group_lock": false,
        "require_two_factor_authentication": false,
        "two_factor_grace_period": 48,
        "project_creation_level": "developer",
        "auto_devops_enabled": null,
        "subgroup_creation_level": "maintainer",
        "emails_disabled": null,
        "mentions_disabled": null,
        "lfs_enabled": true,
        "default_branch_protection": 2,
        "avatar_url": "https://gitlab.com/uploads/-/system/group/avatar/15158087/lock-icon.png",
        "request_access_enabled": true,
        "full_name": "Ultimate Top Five Service / Admin",
        "full_path": "ultimate-top-five/admin",
        "created_at": "2022-01-08T17:56:32.495Z",
        "parent_id": 15143439,
        "ldap_cn": null,
        "ldap_access": null,
        "marked_for_deletion_on": null
      },
      {
        "id": 15143574,
        "web_url": "https://gitlab.com/groups/ultimate-top-five/backoffice-services",
        "name": "backoffice-services",
        "path": "backoffice-services",
        "description": "",
        "visibility": "private",
        "share_with_group_lock": false,
        "require_two_factor_authentication": false,
        "two_factor_grace_period": 48,
        "project_creation_level": "developer",
        "auto_devops_enabled": null,
        "subgroup_creation_level": "maintainer",
        "emails_disabled": null,
        "mentions_disabled": null,
        "lfs_enabled": true,
        "default_branch_protection": 2,
        "avatar_url": "https://gitlab.com/uploads/-/system/group/avatar/15143574/backoffice-logo.png",
        "request_access_enabled": true,
        "full_name": "Ultimate Top Five Service / backoffice-services",
        "full_path": "ultimate-top-five/backoffice-services",
        "created_at": "2022-01-08T01:59:21.043Z",
        "parent_id": 15143439,
        "ldap_cn": null,
        "ldap_access": null,
        "marked_for_deletion_on": null
      },
      {
        "id": 15143573,
        "web_url": "https://gitlab.com/groups/ultimate-top-five/customer-facing",
        "name": "customer-facing",
        "path": "customer-facing",
        "description": "",
        "visibility": "private",
        "share_with_group_lock": false,
        "require_two_factor_authentication": false,
        "two_factor_grace_period": 48,
        "project_creation_level": "developer",
        "auto_devops_enabled": null,
        "subgroup_creation_level": "maintainer",
        "emails_disabled": null,
        "mentions_disabled": null,
        "lfs_enabled": true,
        "default_branch_protection": 2,
        "avatar_url": "https://gitlab.com/uploads/-/system/group/avatar/15143573/customer-facing.png",
        "request_access_enabled": true,
        "full_name": "Ultimate Top Five Service / customer-facing",
        "full_path": "ultimate-top-five/customer-facing",
        "created_at": "2022-01-08T01:58:56.601Z",
        "parent_id": 15143439,
        "ldap_cn": null,
        "ldap_access": null,
        "marked_for_deletion_on": null
      }
    ]"#;

    static SINGLE_PROJECT_JSON_RESPONSE: &str = r#"{
      "id": 32654190,
      "description": "",
      "name": "list-service",
      "name_with_namespace": "Ultimate Top Five Service / backoffice-services / list-service",
      "path": "list-service",
      "path_with_namespace": "ultimate-top-five/backoffice-services/list-service",
      "created_at": "2022-01-08T03:58:52.380Z",
      "default_branch": "main",
      "tag_list": [],
      "topics": [],
      "ssh_url_to_repo": "git@gitlab.com:ultimate-top-five/backoffice-services/list-service.git",
      "http_url_to_repo": "https://gitlab.com/ultimate-top-five/backoffice-services/list-service.git",
      "web_url": "https://gitlab.com/ultimate-top-five/backoffice-services/list-service",
      "readme_url": "https://gitlab.com/ultimate-top-five/backoffice-services/list-service/-/blob/main/README.md",
      "avatar_url": null,
      "forks_count": 0,
      "star_count": 0,
      "last_activity_at": "2022-01-10T07:44:18.036Z",
      "namespace": {
        "id": 15143574,
        "name": "backoffice-services",
        "path": "backoffice-services",
        "kind": "group",
        "full_path": "ultimate-top-five/backoffice-services",
        "parent_id": 15143439,
        "avatar_url": "/uploads/-/system/group/avatar/15143574/backoffice-logo.png",
        "web_url": "https://gitlab.com/groups/ultimate-top-five/backoffice-services"
      },
      "container_registry_image_prefix": "registry.gitlab.com/ultimate-top-five/backoffice-services/list-service",
      "_links": {
        "self": "https://gitlab.com/api/v4/projects/32654190",
        "issues": "https://gitlab.com/api/v4/projects/32654190/issues",
        "merge_requests": "https://gitlab.com/api/v4/projects/32654190/merge_requests",
        "repo_branches": "https://gitlab.com/api/v4/projects/32654190/repository/branches",
        "labels": "https://gitlab.com/api/v4/projects/32654190/labels",
        "events": "https://gitlab.com/api/v4/projects/32654190/events",
        "members": "https://gitlab.com/api/v4/projects/32654190/members"
      },
      "packages_enabled": true,
      "empty_repo": false,
      "archived": false,
      "visibility": "private",
      "resolve_outdated_diff_discussions": false,
      "container_expiration_policy": {
        "cadence": "1d",
        "enabled": false,
        "keep_n": 10,
        "older_than": "90d",
        "name_regex": ".*",
        "name_regex_keep": null,
        "next_run_at": "2022-01-09T03:58:52.394Z"
      },
      "issues_enabled": true,
      "merge_requests_enabled": true,
      "wiki_enabled": true,
      "jobs_enabled": true,
      "snippets_enabled": true,
      "container_registry_enabled": true,
      "service_desk_enabled": true,
      "service_desk_address": "contact-project+ultimate-top-five-backoffice-services-list-service-32654190-issue-@incoming.gitlab.com",
      "can_create_merge_request_in": true,
      "issues_access_level": "enabled",
      "repository_access_level": "enabled",
      "merge_requests_access_level": "enabled",
      "forking_access_level": "enabled",
      "wiki_access_level": "enabled",
      "builds_access_level": "enabled",
      "snippets_access_level": "enabled",
      "pages_access_level": "private",
      "operations_access_level": "enabled",
      "analytics_access_level": "enabled",
      "container_registry_access_level": "enabled",
      "emails_disabled": null,
      "shared_runners_enabled": true,
      "lfs_enabled": true,
      "creator_id": 2380457,
      "import_status": "none",
      "open_issues_count": 1,
      "ci_default_git_depth": 50,
      "ci_forward_deployment_enabled": true,
      "ci_job_token_scope_enabled": false,
      "public_jobs": true,
      "build_timeout": 3600,
      "auto_cancel_pending_pipelines": "enabled",
      "build_coverage_regex": null,
      "ci_config_path": "",
      "shared_with_groups": [],
      "only_allow_merge_if_pipeline_succeeds": false,
      "allow_merge_on_skipped_pipeline": null,
      "restrict_user_defined_variables": false,
      "request_access_enabled": true,
      "only_allow_merge_if_all_discussions_are_resolved": false,
      "remove_source_branch_after_merge": true,
      "printing_merge_request_link_enabled": true,
      "merge_method": "merge",
      "squash_option": "default_off",
      "suggestion_commit_message": null,
      "merge_commit_template": null,
      "squash_commit_template": null,
      "auto_devops_enabled": false,
      "auto_devops_deploy_strategy": "continuous",
      "autoclose_referenced_issues": true,
      "keep_latest_artifact": true,
      "approvals_before_merge": 0,
      "mirror": false,
      "external_authorization_classification_label": "",
      "marked_for_deletion_at": null,
      "marked_for_deletion_on": null,
      "requirements_enabled": false,
      "security_and_compliance_enabled": true,
      "compliance_frameworks": [],
      "issues_template": null,
      "merge_requests_template": null,
      "merge_pipelines_enabled": false,
      "merge_trains_enabled": false
    }
    "#;

    static SINGLE_SUBGROUP_JSON_RESPONSE: &str = r#"{
      "id": 15143574,
      "web_url": "https://gitlab.com/groups/ultimate-top-five/backoffice-services",
      "name": "backoffice-services",
      "path": "backoffice-services",
      "description": "",
      "visibility": "private",
      "share_with_group_lock": false,
      "require_two_factor_authentication": false,
      "two_factor_grace_period": 48,
      "project_creation_level": "developer",
      "auto_devops_enabled": null,
      "subgroup_creation_level": "maintainer",
      "emails_disabled": null,
      "mentions_disabled": null,
      "lfs_enabled": true,
      "default_branch_protection": 2,
      "avatar_url": "https://gitlab.com/uploads/-/system/group/avatar/15143574/backoffice-logo.png",
      "request_access_enabled": true,
      "full_name": "Ultimate Top Five Service / backoffice-services",
      "full_path": "ultimate-top-five/backoffice-services",
      "created_at": "2022-01-08T01:59:21.043Z",
      "parent_id": 15143439,
      "ldap_cn": null,
      "ldap_access": null,
      "marked_for_deletion_on": null,
      "shared_with_groups": [],
      "runners_token": "WkqEr-XJVGRzxVQ2Avhr",
      "projects": [
        {
          "id": 32654190,
          "description": "",
          "name": "list-service",
          "name_with_namespace": "Ultimate Top Five Service / backoffice-services / list-service",
          "path": "list-service",
          "path_with_namespace": "ultimate-top-five/backoffice-services/list-service",
          "created_at": "2022-01-08T03:58:52.380Z",
          "default_branch": "main",
          "tag_list": [],
          "topics": [],
          "ssh_url_to_repo": "git@gitlab.com:ultimate-top-five/backoffice-services/list-service.git",
          "http_url_to_repo": "https://gitlab.com/ultimate-top-five/backoffice-services/list-service.git",
          "web_url": "https://gitlab.com/ultimate-top-five/backoffice-services/list-service",
          "readme_url": "https://gitlab.com/ultimate-top-five/backoffice-services/list-service/-/blob/main/README.md",
          "avatar_url": null,
          "forks_count": 0,
          "star_count": 0,
          "last_activity_at": "2022-01-10T07:44:18.036Z",
          "namespace": {
            "id": 15143574,
            "name": "backoffice-services",
            "path": "backoffice-services",
            "kind": "group",
            "full_path": "ultimate-top-five/backoffice-services",
            "parent_id": 15143439,
            "avatar_url": "/uploads/-/system/group/avatar/15143574/backoffice-logo.png",
            "web_url": "https://gitlab.com/groups/ultimate-top-five/backoffice-services"
          },
          "container_registry_image_prefix": "registry.gitlab.com/ultimate-top-five/backoffice-services/list-service",
          "_links": {
            "self": "https://gitlab.com/api/v4/projects/32654190",
            "issues": "https://gitlab.com/api/v4/projects/32654190/issues",
            "merge_requests": "https://gitlab.com/api/v4/projects/32654190/merge_requests",
            "repo_branches": "https://gitlab.com/api/v4/projects/32654190/repository/branches",
            "labels": "https://gitlab.com/api/v4/projects/32654190/labels",
            "events": "https://gitlab.com/api/v4/projects/32654190/events",
            "members": "https://gitlab.com/api/v4/projects/32654190/members"
          },
          "packages_enabled": true,
          "empty_repo": false,
          "archived": false,
          "visibility": "private",
          "resolve_outdated_diff_discussions": false,
          "container_expiration_policy": {
            "cadence": "1d",
            "enabled": false,
            "keep_n": 10,
            "older_than": "90d",
            "name_regex": ".*",
            "name_regex_keep": null,
            "next_run_at": "2022-01-09T03:58:52.394Z"
          },
          "issues_enabled": true,
          "merge_requests_enabled": true,
          "wiki_enabled": true,
          "jobs_enabled": true,
          "snippets_enabled": true,
          "container_registry_enabled": true,
          "service_desk_enabled": true,
          "service_desk_address": "contact-project+ultimate-top-five-backoffice-services-list-service-32654190-issue-@incoming.gitlab.com",
          "can_create_merge_request_in": true,
          "issues_access_level": "enabled",
          "repository_access_level": "enabled",
          "merge_requests_access_level": "enabled",
          "forking_access_level": "enabled",
          "wiki_access_level": "enabled",
          "builds_access_level": "enabled",
          "snippets_access_level": "enabled",
          "pages_access_level": "private",
          "operations_access_level": "enabled",
          "analytics_access_level": "enabled",
          "container_registry_access_level": "enabled",
          "emails_disabled": null,
          "shared_runners_enabled": true,
          "lfs_enabled": true,
          "creator_id": 2380457,
          "import_status": "none",
          "open_issues_count": 1,
          "ci_default_git_depth": 50,
          "ci_forward_deployment_enabled": true,
          "ci_job_token_scope_enabled": false,
          "public_jobs": true,
          "build_timeout": 3600,
          "auto_cancel_pending_pipelines": "enabled",
          "build_coverage_regex": null,
          "ci_config_path": "",
          "shared_with_groups": [],
          "only_allow_merge_if_pipeline_succeeds": false,
          "allow_merge_on_skipped_pipeline": null,
          "restrict_user_defined_variables": false,
          "request_access_enabled": true,
          "only_allow_merge_if_all_discussions_are_resolved": false,
          "remove_source_branch_after_merge": true,
          "printing_merge_request_link_enabled": true,
          "merge_method": "merge",
          "squash_option": "default_off",
          "suggestion_commit_message": null,
          "merge_commit_template": null,
          "squash_commit_template": null,
          "auto_devops_enabled": false,
          "auto_devops_deploy_strategy": "continuous",
          "autoclose_referenced_issues": true,
          "keep_latest_artifact": true,
          "approvals_before_merge": 0,
          "mirror": false,
          "external_authorization_classification_label": "",
          "marked_for_deletion_at": null,
          "marked_for_deletion_on": null,
          "requirements_enabled": false,
          "security_and_compliance_enabled": true,
          "compliance_frameworks": [],
          "issues_template": null,
          "merge_requests_template": null,
          "merge_pipelines_enabled": false,
          "merge_trains_enabled": false
        },
        {
          "id": 32654182,
          "description": "",
          "name": "profile-service",
          "name_with_namespace": "Ultimate Top Five Service / backoffice-services / profile-service",
          "path": "profile-service",
          "path_with_namespace": "ultimate-top-five/backoffice-services/profile-service",
          "created_at": "2022-01-08T03:57:55.601Z",
          "default_branch": "main",
          "tag_list": [],
          "topics": [],
          "ssh_url_to_repo": "git@gitlab.com:ultimate-top-five/backoffice-services/profile-service.git",
          "http_url_to_repo": "https://gitlab.com/ultimate-top-five/backoffice-services/profile-service.git",
          "web_url": "https://gitlab.com/ultimate-top-five/backoffice-services/profile-service",
          "readme_url": "https://gitlab.com/ultimate-top-five/backoffice-services/profile-service/-/blob/main/README.md",
          "avatar_url": null,
          "forks_count": 0,
          "star_count": 0,
          "last_activity_at": "2022-01-08T20:23:09.934Z",
          "namespace": {
            "id": 15143574,
            "name": "backoffice-services",
            "path": "backoffice-services",
            "kind": "group",
            "full_path": "ultimate-top-five/backoffice-services",
            "parent_id": 15143439,
            "avatar_url": "/uploads/-/system/group/avatar/15143574/backoffice-logo.png",
            "web_url": "https://gitlab.com/groups/ultimate-top-five/backoffice-services"
          },
          "container_registry_image_prefix": "registry.gitlab.com/ultimate-top-five/backoffice-services/profile-service",
          "_links": {
            "self": "https://gitlab.com/api/v4/projects/32654182",
            "issues": "https://gitlab.com/api/v4/projects/32654182/issues",
            "merge_requests": "https://gitlab.com/api/v4/projects/32654182/merge_requests",
            "repo_branches": "https://gitlab.com/api/v4/projects/32654182/repository/branches",
            "labels": "https://gitlab.com/api/v4/projects/32654182/labels",
            "events": "https://gitlab.com/api/v4/projects/32654182/events",
            "members": "https://gitlab.com/api/v4/projects/32654182/members"
          },
          "packages_enabled": true,
          "empty_repo": false,
          "archived": false,
          "visibility": "private",
          "resolve_outdated_diff_discussions": false,
          "container_expiration_policy": {
            "cadence": "1d",
            "enabled": false,
            "keep_n": 10,
            "older_than": "90d",
            "name_regex": ".*",
            "name_regex_keep": null,
            "next_run_at": "2022-01-09T03:57:55.618Z"
          },
          "issues_enabled": true,
          "merge_requests_enabled": true,
          "wiki_enabled": true,
          "jobs_enabled": true,
          "snippets_enabled": true,
          "container_registry_enabled": true,
          "service_desk_enabled": true,
          "service_desk_address": "contact-project+ultimate-top-five-backoffice-services-profile-service-32654182-issue-@incoming.gitlab.com",
          "can_create_merge_request_in": true,
          "issues_access_level": "enabled",
          "repository_access_level": "enabled",
          "merge_requests_access_level": "enabled",
          "forking_access_level": "enabled",
          "wiki_access_level": "enabled",
          "builds_access_level": "enabled",
          "snippets_access_level": "enabled",
          "pages_access_level": "private",
          "operations_access_level": "enabled",
          "analytics_access_level": "enabled",
          "container_registry_access_level": "enabled",
          "emails_disabled": null,
          "shared_runners_enabled": true,
          "lfs_enabled": true,
          "creator_id": 2380457,
          "import_status": "none",
          "open_issues_count": 1,
          "ci_default_git_depth": 50,
          "ci_forward_deployment_enabled": true,
          "ci_job_token_scope_enabled": false,
          "public_jobs": true,
          "build_timeout": 3600,
          "auto_cancel_pending_pipelines": "enabled",
          "build_coverage_regex": null,
          "ci_config_path": "",
          "shared_with_groups": [],
          "only_allow_merge_if_pipeline_succeeds": false,
          "allow_merge_on_skipped_pipeline": null,
          "restrict_user_defined_variables": false,
          "request_access_enabled": true,
          "only_allow_merge_if_all_discussions_are_resolved": false,
          "remove_source_branch_after_merge": true,
          "printing_merge_request_link_enabled": true,
          "merge_method": "merge",
          "squash_option": "default_off",
          "suggestion_commit_message": null,
          "merge_commit_template": null,
          "squash_commit_template": null,
          "auto_devops_enabled": false,
          "auto_devops_deploy_strategy": "continuous",
          "autoclose_referenced_issues": true,
          "keep_latest_artifact": true,
          "approvals_before_merge": 0,
          "mirror": false,
          "external_authorization_classification_label": "",
          "marked_for_deletion_at": null,
          "marked_for_deletion_on": null,
          "requirements_enabled": false,
          "security_and_compliance_enabled": true,
          "compliance_frameworks": [],
          "issues_template": null,
          "merge_requests_template": null,
          "merge_pipelines_enabled": false,
          "merge_trains_enabled": false
        }
      ],
      "shared_projects": [],
      "shared_runners_minutes_limit": null,
      "extra_shared_runners_minutes_limit": null,
      "prevent_forking_outside_group": true
    }

    "#;

    static VERSION_RESPONSE: &str = r#"{
      "version": "14.0.0-xxx",
      "revision": "cbfa38e64f1"
    }"#;

    #[derive(Debug, Serialize, Deserialize)]
    struct SimpleUser {
        name: String,
    }

    #[test]
    fn test_simple_array_deserialization() {
        let arr_str = "[ { \"name\": \"Mike\"} ]";

        let result: Vec<SimpleUser> = serde_json::from_str(&arr_str).unwrap();

        assert_eq!(result.len(), 1);
        let user = result.get(0).unwrap();
        assert_eq!(user.name, "Mike");
    }

    #[test]
    fn test_subgroup_deserialization() {
        let sgs: SubGroup = serde_json::from_str(SUBGROUP_JSON_RESPONSE).unwrap();

        assert_eq!(sgs.id, 15143573);
    }

    #[test]
    fn test_full_subgroups_response_deserialization() {
        let subgroups: Vec<SubGroup> = serde_json::from_str(SUBGROUPS_JSON_RESPONSE).unwrap();

        assert_eq!(subgroups.len(), 3);
        for subgroup in subgroups {
            assert!(!subgroup.full_path.is_empty());
        }
    }

    #[test]
    fn test_version() {
        let version: Version = serde_json::from_str(VERSION_RESPONSE).unwrap();

        assert_eq!(version.version, "14.0.0-xxx");
        assert_eq!(version.revision, "cbfa38e64f1");
    }

    #[test]
    fn test_project() {
        let project: Project = serde_json::from_str(SINGLE_PROJECT_JSON_RESPONSE).unwrap();

        assert_eq!(project.id, 32654190);
        assert_eq!(
            project.repo_url,
            "git@gitlab.com:ultimate-top-five/backoffice-services/list-service.git"
        );
    }

    #[test]
    fn test_subgroup_projects() {
        let subgroup: SubGroupProjects =
            serde_json::from_str(SINGLE_SUBGROUP_JSON_RESPONSE).unwrap();

        assert_eq!(subgroup.projects.len(), 2);
        assert_eq!(subgroup.subgroup_name, "backoffice-services");
    }
}
