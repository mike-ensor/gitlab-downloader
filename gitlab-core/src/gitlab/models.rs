use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct Project {
    pub name: String,
    pub id: i32,
    #[serde(rename(deserialize = "ssh_url_to_repo"))]
    pub repo_url: String, // ssh_url_to_repo
    #[serde(rename(deserialize = "path_with_namespace"))]
    pub repo_path: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct SubGroup {
    pub id: i32,
    pub name: String,
    pub path: String,
    pub full_path: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Version {
    pub version: String,
    pub revision: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct SubGroupProjects {
    pub id: i32,
    #[serde(rename(deserialize = "name"))]
    pub subgroup_name: String,
    #[serde(rename(deserialize = "projects"))]
    pub projects: Vec<Project>,
}
