#[cfg(test)]
mod tests {
    use crate::cli_config::{CloneCommand, CommonConfig};
    use crate::gitlab::gitlab_client::GitLabClient;
    use mockito::{mock, reset, Mock};
    use reqwest::blocking::Client;
    use std::fs;
    use std::net::SocketAddr;

    #[test]
    fn test_new_with_defaults() {
        let token = "my-token-here".to_owned();
        let group_id = 12345;
        let expected_insecure = false;
        let expected_host = "gitlab.com".to_owned();
        let expected_max_depth: usize = 0;
        let expected_path = ".";
        let expected_dry_run = false;

        let mut common_config = CommonConfig::new(token.clone(), "/tmp".to_string());

        let command = CloneCommand {
            group_id,
            max_depth: 0,
            config: common_config,
        };

        let config = GitLabClient::new(command);

        assert_eq!(config.token(), token, "Tokens should be equal");
        assert_eq!(config.group_id(), group_id, "Group IDs should be equal");
    }

    #[test]
    fn test_clone_can_access_api_success() {
        let token = "2345232";
        let group_id = "23456".parse::<usize>().unwrap();
        let expected_version = "14.0.0-xxx"; // TODO: Copied into multiline until can figure interpolation with multiline
        let expected_revision = "cbfa38e64f1";

        let mock_body_response = r#"{
              "version": "14.0.0-xxx",
              "revision": "cbfa38e64f1"
            }
        "#;

        let (host, mock) = mock_call(token, "/api/v4/version", mock_body_response);

        let mut common_config = CommonConfig::new("2345232".to_string(), "/tmp".to_string());
        common_config.insecure = true;
        common_config.host = host;

        let clone_config = CloneCommand {
            group_id,
            max_depth: 0,
            config: common_config,
        };

        let mut config = GitLabClient::new(clone_config);

        let result = config.api_version();
        assert_eq!(result.is_ok(), true);
        let version = result.unwrap();
        assert_eq!(version.version, expected_version);
        assert_eq!(version.revision, expected_revision);
        mock.assert();

        reset();
    }

    fn mock_call(token: &str, path: &str, mock_response: &str) -> (String, Mock) {
        let socket = mockito::server_address();
        let mock_version = mock("GET", path)
            .with_status(200)
            .with_header("content-type", "application/json")
            .with_header("PRIVATE-TOKEN", token)
            .with_body(mock_response)
            .expect(1)
            .create();
        let host = format!("{}:{}", socket.ip().to_string(), socket.port());
        (host, mock_version)
    }
}
