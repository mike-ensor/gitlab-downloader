use crate::cli_config::CloneCommand;
use crate::gitlab::models::{Project, SubGroup, SubGroupProjects, Version};
use crate::gitlab::GitLabErrors;
use git2::build::RepoBuilder;
use git2::{Config, Cred, CredentialType, Error, FetchOptions, RemoteCallbacks, Repository};
use git2_credentials::ui4dialoguer::CredentialUI4Dialoguer;
use git2_credentials::CredentialHandler;
use log::{debug, error, info};
use reqwest::blocking::{Client, ClientBuilder, Response};
use std::any::Any;
use std::env;
use std::path::Path;
use std::time::Duration;

pub struct GitLabClient {
    client: Client,
    pub(crate) command: CloneCommand, //TODO: When generic group of any command, fix this
}

/*
/groups/:id/projects - lists all projects of a group // https://docs.gitlab.com/ee/api/groups.html#list-a-groups-projects
/groups/:id/subgroups - lists all subgroups of a group // want "path_with_namespace" for the path
 */

/// Private convenience struct used to store which repos need cloning and the status after running
#[derive(Clone)]
struct CloneData {
    repo_path: String,
    repo_url: String,
    os_path: Option<String>,
    run_error: Option<GitLabErrors>,
}

impl CloneData {
    pub fn add_error(&mut self, err: GitLabErrors) {
        self.run_error = Some(err);
    }

    pub fn is_error(&self) -> bool {
        self.run_error.is_some()
    }

    pub fn get_error(&self) -> &GitLabErrors {
        self.run_error.as_ref().unwrap()
    }
}

impl GitLabClient {
    pub fn new(command: CloneCommand) -> GitLabClient {
        GitLabClient {
            client: Client::new(),
            command,
        }
    }

    pub fn token(&self) -> String {
        self.command.config.token.clone()
    }

    pub fn group_id(&self) -> usize {
        self.command.group_id
    }

    pub(crate) fn api_version(&self) -> Result<Version, GitLabErrors> {
        let result = self.get_endpoint(String::from("version"));

        Self::get_version(result)
    }

    // TODO: What to return? Vec<String> (the paths)?
    /// Clones all of the repos that a user has access to into a Directory representation of the GitLab Group tree
    pub fn clone_repos(&self) -> Vec<GitLabErrors> {
        // collection of CloneData objects that will be performed (maybe in parallel)
        let mut repo_data: Vec<CloneData> = vec![];
        let dry_run = self.command.config.dry_run;

        // Get projects at the Group (root at this point)
        let group_id = self.command.group_id as i32;
        self.get_projects_for_group(&mut repo_data, group_id);

        // Get all SubGroups
        let endpoint_uri = format!("/groups/{}/subgroups", self.command.group_id);
        let subgroup_response = self.get_endpoint(endpoint_uri);

        let subgroups = self.get_subgroups(subgroup_response).unwrap(); // TODO: CHECK FOR ERRORS
        for subgroup in subgroups {
            self.get_projects_for_group(&mut repo_data, subgroup.id);
        }

        let mut errors: Vec<GitLabErrors> = vec![];
        // Perform actual clone
        for mut repo in repo_data {
            if repo.os_path.is_some() {
                let os_path = repo.os_path.clone();
                let host_path = os_path.unwrap();

                if dry_run {
                    println!("DRY-RUN: Clone [{}] into [{}]", repo.repo_url, host_path);
                } else {
                    // Create callbacks function for credentials check (each repo "may" have different credentials, which is why this inside the loop
                    let mut callbacks = RemoteCallbacks::new();
                    let git_config = git2::Config::open_default().unwrap();
                    let mut ch = CredentialHandler::new(git_config);

                    // function uses credential helper to try all SSH config entries (kinda a brute force, but doesn't require a user to specify IdentifyFile or keys)
                    callbacks.credentials(move |url, username, allowed| {
                        ch.try_next_credential(url, username, allowed)
                    });

                    // associate callbacks with Git FetchOptions
                    let mut fetch_opts = FetchOptions::new();
                    fetch_opts
                        .remote_callbacks(callbacks)
                        .download_tags(git2::AutotagOption::All)
                        .update_fetchhead(true);

                    let clone_response = RepoBuilder::new()
                        .fetch_options(fetch_opts)
                        .clone(repo.repo_url.as_str(), Path::new(&host_path));

                    // handle the response back into CloneData
                    let repo_resp = match clone_response {
                        Ok(repo_resp) => {
                            println!("Cloned {} to {}", repo.repo_url, host_path);
                            repo
                        }
                        Err(e) => {
                            let repo_prefixed = format!("ssh://{}", repo.repo_url);
                            let clone_error = GitLabErrors::FailedClone(format!(
                                "failed to clone {}: {}",
                                repo_prefixed,
                                e.message()
                            ));
                            println!("{clone_error}");
                            // repo.add_error(clone_error);
                            errors.push(clone_error);
                            repo
                        }
                    };
                }
            }
        } // end of executing on all repos

        // repo_data
        errors
    }

    fn get_projects(&self, result: reqwest::Result<Response>) -> Vec<Project> {
        let projects: Vec<Project> = match result {
            Ok(res) => match res.status() {
                reqwest::StatusCode::OK => {
                    let str = res.text().unwrap();
                    let projects_group: Vec<Project> = serde_json::from_str(&str).unwrap();
                    projects_group
                }
                _ => {
                    error!(
                        "Status code failed trying to extract Projects from Subgroup: {}",
                        res.status()
                    );
                    vec![]
                }
            },
            Err(err) => {
                error!("Failed to get response: {}", err);
                vec![]
            }
        };

        projects
    }

    fn get_projects_for_group(&self, repo_data: &mut Vec<CloneData>, project_id: i32) {
        let endpoint = format!("/groups/{}/projects", project_id);
        let projects_response = self.get_endpoint(endpoint);
        let projects = self.get_projects(projects_response);

        for project in projects {
            // Only add a CloneData if the OS Path is valid
            if self.command.config.os_path.is_ok() {
                debug!("Project {}, path {}", project.name, project.repo_path);
                let os_path = self.command.config.os_path.clone();
                let host_path = format!("{}/{}", os_path.ok().unwrap(), project.repo_path);
                repo_data.push(CloneData {
                    repo_url: project.repo_url,
                    repo_path: project.repo_path,
                    os_path: Some(host_path),
                    run_error: Option::None,
                });
            }
        }
    }

    /// Private function to call GitLab's API for the current API Version and Revision
    fn get_version(result: reqwest::Result<Response>) -> Result<Version, GitLabErrors> {
        return match result {
            Ok(res) => match res.status() {
                reqwest::StatusCode::OK => {
                    let str = res.text().unwrap();
                    let version: Version = serde_json::from_str(&str).unwrap();
                    Ok(version)
                }
                _ => Err(GitLabErrors::HttpError(
                    res.status().as_u16(),
                    "Unable to get a response from endpoint".to_string(),
                )),
            },
            Err(err) => Err(GitLabErrors::Connection(format!(
                "Failed to get response ({err})"
            ))),
        };
    }

    /// Gets the subgroups from a response type, if none or error, returns empty
    /// TODO: Figure out how to make this generic like `get_version` above
    fn get_subgroups(&self, response: reqwest::Result<Response>) -> Result<Vec<SubGroup>, String> {
        return match response {
            Ok(res) => match res.status() {
                reqwest::StatusCode::OK => {
                    let str = res.text().unwrap();
                    let sgs: Vec<SubGroup> = serde_json::from_str(&str).unwrap();
                    Ok(sgs)
                }
                _ => Err(format!("Status code failed {}", res.status())),
            },
            Err(err) => Err(format!("Failed to get response: {}", err)),
        };
    }

    // Performs the API call
    fn get_endpoint(&self, endpoint: String) -> reqwest::Result<Response> {
        let protocol = if self.command.config.insecure {
            "http"
        } else {
            "https"
        };
        let host = &self.command.config.host;
        let token = &self.command.config.token;

        let request_url = format!("{}://{}/api/v4/{}", protocol, host, endpoint);

        debug!("Querying Endpoint: {:?}", endpoint);

        self.client
            .get(request_url)
            .header("Accept", "application/json")
            .header("PRIVATE-TOKEN", token)
            .timeout(Duration::from_secs(5))
            .send()
    }
}
