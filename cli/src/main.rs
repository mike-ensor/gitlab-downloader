#![allow(unused, dead_code)]

extern crate gitlab_core as core;

use std::borrow::Borrow;
use std::io;
use std::ops::Deref;
use std::str::FromStr;
use std::string::ParseError;

use clap::{App, AppSettings, Arg, ArgMatches};
use clap_complete::generate;
use clap_complete::Shell::Bash;
use log::debug;

use gitlab_core::cli_config::{CliError, CloneCommand, Command, CommonConfig, Update};

mod main_test;

fn main() {
    env_logger::init();
    // get the input from the user & automatically fail if inputs are not correct/satisfied
    let mut app = get_app(get_host_args(), get_token_args());

    // generate(Bash, &mut app, "myapp", &mut io::stdout());

    let matches_result = app.get_matches();
    let matches = matches_result;

    // Validate input for each subgroup before making any actions
    let command = get_command(matches);

    if let Err(e) = command {
        println!("{}", e);
    } else {
        let cmd = command.unwrap();

        if let Err(err) = cmd.is_valid() {
            println!("{err}");
        } else {
            println!("Fetching info...");
            let result = cmd.perform();
            match result {
                Ok(_) => println!("Complete!"),
                Err(err) => println!("There were errors: {}", err),
            }
        }
    }
}

// TODO: How do I make this generic?
fn get_command(matches: ArgMatches) -> Result<Box<CloneCommand>, CliError> {
    let clone_command = match matches.subcommand_matches("clone") {
        None => None,
        Some(sub_args) => {
            // REQUIRED
            let group_id: usize = get_group_id(sub_args)?;

            // REQUIRED
            let token = match sub_args.value_of("token") {
                // NOTE: Content/value is checked inside the object, we're only collecting the CLI params
                None => String::new(),
                Some(val) => String::from(val),
            };

            let os_path = match sub_args.value_of("path") {
                None => String::new(),
                Some(val) => String::from(val),
            };

            // create configuration from input for Clone
            let common_config = CommonConfig::new(token, os_path);
            let clone_command = CloneCommand {
                group_id,
                max_depth: 0,
                config: common_config,
            };

            Some(clone_command)
        }
    };

    // let update_command: Option<Update> = match matches.subcommand_matches("update") {
    //     None => None,
    //     Some(update) => {
    //         // update.value_of()
    //         // Err("Not Implemented Yet".to_string())
    //         None
    //     }
    // };
    // one of these is going to be it
    // let new_command = CloneCommand::from(clone_command.unwrap());

    Ok(Box::new(clone_command.unwrap())) // IGNORING ERRORS FOR NOW
}

/// Extract the GroupID from the matches & validate
fn get_group_id(matches: &ArgMatches) -> Result<usize, CliError> {
    match matches.value_of("group_id") {
        None => Err(CliError::GroupId(
            // this should never be triggered IF the Clap configuration is correct
            "'GroupID' is a required parameter".to_string(),
        )),
        Some(val) => {
            let conversion = val.parse::<usize>();
            match conversion {
                Ok(val) => Ok(val),
                Err(_) => Err(CliError::GroupId(format!(
                    "Group IDs should be numbers only. '{}' is not a valid input",
                    val
                ))),
            }
        }
    }
}

/// Returns an App object for CLI
// TODO: Look into swapping for YAML config version instead
fn get_app<'a>(host_arg: Arg<'a>, token_arg: Arg<'a>) -> App<'a> {
    App::new("Gitlab Downloader")
        .version("0.1")
        .setting(AppSettings::SubcommandRequiredElseHelp)
        .about("This clones all (or filtered) repos from a Parent Group. Also updates all repos in subgroups")
        // .arg(Arg::new("v")
        //     .short('v')
        //     .multiple(true)
        //     .help("Sets the level of verbosity"))
        .subcommand(App::new("clone")
            .about("Clones repositories in and under Group ID")
            .arg(&token_arg)
            .arg(&host_arg)
            .arg(Arg::new("group_id")
                .short('g')
                .long("group-id")
                .value_name("GROUPID")
                .help("Root/Parent GroupID")
                .long_help("Provide the Gitlab Root Group ID. CLI overrides ENV var, one is required -- ($GITLAB_DOWNLOADER_ROOT_GROUP_ID)")
                .required(true)
                .takes_value(true))
            .arg(Arg::new("path")
                .help("(Optional) OS Path to clone repositories and subgroups into")
                .long_help("(Optional) OS Path to clone repositories and subgroups into defaulting to the current working directory")
                // .default_missing_value(".")
                .required(false)
                .takes_value(true)
                .value_name("PATH")
                .index(1)
            )
            .arg(Arg::new("insecure")
                .long("insecure")
                .short('i')
                .help("(Optional) Host connection uses insecure (http vs https)")
                .required(false)
                .takes_value(false)
            )
            .arg(Arg::new("dryrun")
                .long("dry-run")
                .help("Only print what actions would happen")
                .required(false)
                .takes_value(false)
            )
            .arg(Arg::new("quiet")
                .short('q')
                .long("quiet")
                .help("Suppress all output to standard out")
                .required(false)
                .takes_value(false)
            )
            .arg(Arg::new("depth")
                .short('d')
                .long("depth")
                .long_help("Limit the levels to clone")
                .help("How many levels to go")
                .default_value("All")
            ))
        .subcommand(App::new("update")
            .about("Update/Pull changes on all repos and subgroup's repos")
            .long_about("\nUpdates each project in the folder and sub-folders using `git pull`\n\nNOTE: All conflicts need to be updated manually")
            .arg(&token_arg)
        )
}

fn get_token_args() -> Arg<'static> {
    let token_arg = Arg::new("token")
        .short('t')
        .long("token")
        .value_name("TOKEN")
        .help("(Optional) Gitlab Access Token")
        .long_help("Provide a Gitlab Access Token to either overwrite GITLAB_DOWNLOADER_TOKEN or provide a value. Either the ENV variable or CLI argument is required")
        .required(true)
        .takes_value(true);
    token_arg
}

fn get_host_args() -> Arg<'static> {
    let host_arg = Arg::new("host")
        .long("host")
        .value_name("HOST")
        .help("(Optional) Gitlab domain hostname")
        .long_help("If the endpoint is not SaaS, provide a hostname (TLD) or use the ENV variable GITLAB_DOWNLOADER_HOST. Either the ENV variable or CLI argument is required if not using SaaS GitLab")
        .required(false)
        .takes_value(true);
    host_arg
}
