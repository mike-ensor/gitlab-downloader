#[cfg(test)]
mod tests {
    use std::env;

    use assert_cmd::Command;
    use clap::ArgMatches;

    use crate::{get_app, get_group_id, get_host_args, get_token_args};

    use super::*;

    #[test]
    fn it_default_command() {
        // Test should result in an error (or series of errors eventually)
        let mut cmd = Command::cargo_bin("gitlab-downloader").unwrap();
        cmd.assert().failure().code(2);
    }

    #[test]
    fn it_test_clone_help() {
        // test should result in 0 exit (ok) because `gitlab-downloader clone --help` is valid`
        let mut cmd = Command::cargo_bin("gitlab-downloader").unwrap();

        cmd.write_stdin("")
            .arg("clone")
            .arg("--help")
            .arg("--group-id 123")
            .arg("--token abcdefghijklmnop")
            .assert()
            .success()
            .code(0);
    }

    #[test]
    fn test_get_group_id() {
        let expected_group = "12345".to_owned();
        let expected_group_usize = expected_group.parse::<usize>().unwrap();

        let arg_vec = vec![
            "gitlab-downloader",
            "clone",
            "--token",
            "default-token-value",
            "--group-id",
            &expected_group,
        ];

        let mut matches =
            get_app(get_host_args(), get_token_args()).try_get_matches_from_mut(arg_vec);

        let args = matches.unwrap().clone();
        let subcommand = args.subcommand_matches("clone").unwrap();
        let group_id = subcommand.value_of("group_id").unwrap();

        assert_eq!(group_id, &expected_group);

        let second_group = get_group_id(&subcommand);
        assert!(second_group.is_ok());
        assert_eq!(second_group.unwrap(), expected_group_usize);
    }

    #[test]
    fn test_current_working_directory() {
        let curr_dir = env::current_dir().unwrap();
        let path = curr_dir.as_path();
        let path_os = path.as_os_str();

        let path_string = String::from(path.to_str().unwrap());
        let path_string_two = String::from(path_os.to_str().unwrap());
        let path_string_three = String::from(curr_dir.as_os_str().to_str().unwrap());

        println!("{path_string}");
        println!("{path_string_two}");
        println!("{path_string_three}");
    }
}
