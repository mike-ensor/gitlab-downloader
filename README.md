# Overview

This project creates a binary for multiple architectures that clones and pulls upstream changes from GitLab projects
that are stored in a [multi-group structure](https://docs.gitlab.com/ee/user/group/subgroups/).

# Commands

> NOTE: Only repositories are cloned or updated when the provided `access-token` (PAT) has visibility and rights to clone or update

| Command  | Description                                                                                                                                          | Flags                                                                                                                                                                                                                                                    |
|:---------|:-----------------------------------------------------------------------------------------------------------------------------------------------------|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `clone`  | Clones repositories at the group level and subgroups.<br/><br/>NOTE: Future iterations will be able to derive the `root-group-id` via a provided URL | Root Group ID (`root-group-id`: _integer_) -- The number corresponding to the parent [Group ID][2] of which all repos and subgroups exist.<br/><br/>Depth (`--max-depth`: _integer_) provides a maximum depth to traverse [subgroups][3]. Default is all |
| `update` | Updates each project in the folder and sub-folders using `git pull`<br/><br/>NOTE: All conflicts need to be updated manually                         | Depth (`--max-depth`: _integer_) provides a maximum depth to traverse [subgroups][3]. Default is all                                                                                                                                                     |

# Configuration

General configuration for the tooling (provided by either command-line switch or ENV variable in this precedence)

> :warning: ENV variables as configuration are not yet functional (primarily due to testing parallelization resource contention in Rust. Less dependent solution coming in soon so ENVs will be back)

| Option        | Description                                              | Flag                            | Required? | Default    | Equiv ENV                  |
|:--------------|----------------------------------------------------------|:--------------------------------|:---------:|:-----------|:---------------------------|
| Directory     | Relative or absolute path to clone in to or update from  | `--directory=<path>`            |    No     | ./         | N/A                        |
| Host          | URL of the host providing the GitLab API                 | `--host=<hostURL>`              |    No     | gitlab.com | GITLAB_DOWNLOADER_HOST     |
| Access Token  | Access token [Personal Access Token][1] with `api` scope | `--token=<secret-token-string>` |    Yes    | n/a        | GITLAB_DOWNLOADER_TOKEN    |
| Insecure Host | Use http instead of https for API host                   | `--insecure`                    |    No     | false      | GITLAB_DOWNLOADER_INSECURE |


# Examples

> :warning: This is not yet functional. For the time-being, please use `gitlab-downloader <command> --token ${TOKEN} --group-id ${GROUP_ID}`

1. All defaults, but using Environment Variables for `token`

```bash
export GITLAB_DOWNLOADER_TOKEN="token-value-here"
gitlab-downloader clone
 ```

1. Using defaults, but supplying `token` as command-line parameter (specifying directory path)

```bash
gitlab-downloader clone --token="token-value-here" --directory=/tmp/some-folder

# OR

gitlab-downloader clone --token="${TOKEN}" --directory=/tmp/some-folder
```

1. Supplying Host and Token via ENV variable
```bash
export GITLAB_DOWNLOADER_TOKEN="token-value-here"
export GITLAB_DOWNLOADER_HOST="gitlab.company.com"
gitlab-downloader clone
```

1. Supplying Host and Token via command-line
```bash
gitlab-downloader clone --token="token-value-here" --host="gitlab.company.com"
```


## Testing

```bash
export GITLAB_TOKEN="token-value-here" # personal access token
export HOST="gitlab.com" # or some private instance

export API_ENDPOINT="groups/"

curl -s --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://${HOST}/api/v4/${API_ENDPOINT}"
```



[1]: https://gitlab.example.com/-/profile/personal_access_tokens?name=Example+Access+token&scopes=api
[2]: https://docs.gitlab.com/ee/user/group/
[3]: https://docs.gitlab.com/ee/user/group/subgroups/