# 1. New approach to project and structure

Date: Jan-15-2022

## Status

Accepted

## Context

The initial approach at the CLI was very linear and brute force. The primary reason for developing this is to learn Rust with a hands-on approach, but now that I have learned more, this approach is not right and is untenable.

### Drivers

* Cyclomatic complexity is high (not measured, just observed)
* Very difficult to test
* Lack of Rust knowledge and the "way to do things in Rust" when starting project
* Dependencies were creeping up and thus compile time too (no need to re-compile everything)

## Considered Options

* Keep going until it was unusable

## Decision

Switch to `workspaces` and divide up into a Library and Binary artifact. 

### Main(bin)

This crate will handle the user interaction with the library where the commands will be run. The primary goal of this binary is to capture the input parameters (and config file), figure out which command needs to be run in the library, and handle success/error. Validation of user input data is also a goal.

### GitLab Core(lib)

This crate will handle the operations of the commands by interacting with the GitLab API and Operating System.

## Consequences

This is a bit of work, it'll take some time and continue to ferret out more challenges with lack of Rust knowledge (all good though).

## Goals
* Create a CLI to download (someday update) GitLab repos contained within a Group (via GroupID)
* Create a CLI for Linux(x86), Windows and MacOS