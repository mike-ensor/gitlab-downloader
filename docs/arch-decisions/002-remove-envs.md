# 2. Remove Environment Variables

Date: Jan-22-2022

## Status

Accepted

## Context

Environment variables are a nice-to-have, but are shared across the system at any time.
This complicates parallel testing to a point where it's just not useful at this
point in the product process and should be removed, then added in later as a convenience.

### Drivers

* Parallel test runner for Rust
* Concurrent use of ENV vars across tests

## Considered Options

* Single threaded test (slows down testing)
* Mutex (would provide a good blog)
* Mark ignore and individually run each when manually triggering testing

## Decision

Remove Environment Variables for CLI tooling in the first iteration of the project.
Revisit the decision and implement one of the below ideas (if not new ones found)

## Consequences

Each time the command is run to clone repos, all parameters need to be specified in the CLI arguments. 
This will feel like an inconvenience if the command is run multiple times (especially important when "update" is being completed)