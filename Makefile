
# cargo-x plugin only works within a single workspace. This makefile is needed to run the same commands that would have been run.
#   This is not a complex makefile, only used to run commands that would have been cargo-x commands

current_dir := $(shell pwd)

cargo-build:
	cargo build

pre-commit: cargo-build
	cargo test --features include_integration_tests && cargo outdated && cargo audit && cargo clippy -- -D warnings && cargo fmt -- --check

deploy-local: cargo-build undeploy-local
	ln -s ${current_dir}/target/x86_64-unknown-linux-gnu/debug/gitlab-downloader ${HOME}/.local/bin/gitlab-downloader

undeploy-local: cargo-build
	rm -rf ${HOME}/.local/bin/gitlab-downloader && true

sanity-test: cargo-build
	target/x86_64-unknown-linux-gnu/debug/gitlab-downloader clone --group-id ${GITLAB_DOWNLOADER_ROOT_GROUP_ID} --token ${GITLAB_DOWNLOADER_TOKEN} /tmp/gitlab-downloader-YCK